#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import argparse
import pexpect
import time
import subprocess
import math
import operator
import functools
from PIL import Image

# Logging stuff
import logging

logger = logging.getLogger("logging_tryout2")
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter(
    "[{asctime} | {levelname:5s} ] --- {message}", "%Y-%m-%d %H:%M:%S", "{"
)

ch.setFormatter(formatter)
logger.addHandler(ch)

#


DEBUG_COLOR = "1;33"
INFO_COLOR = "39"
CPU = 6


def compare_screen(vnc_port, screen):
    i1 = Image.open(screen)
    base = screen.split(".jpg")[0]
    pexpect.run(f"vncdo -s localhost:{vnc_port} capture {base}_test.jpg")
    i2 = Image.open(f"{base}_test.jpg")
    h1 = i1.histogram()
    h2 = i2.histogram()
    rms = math.sqrt(
        functools.reduce(operator.add, map(lambda a, b: (a - b) ** 2, h1, h2)) / len(h1)
    )
    pexpect.run("rm ./{base}_test.jpg")
    return rms


def test_windows_image(path, imFormat, port=1, verbose=False, suffix=".results"):
    testtimeout = 40
    timebetween = 5
    logger.info(f"\x1b[{INFO_COLOR}mTesting Windows image from path {path}\x1b[0m")
    logger.debug(f"\x1b[{DEBUG_COLOR}mstarting pywb ...\x1b[0m")
    docker = pexpect.spawn("docker-compose up")
    docker.expect(".*running gevent loop engine.*", timeout=120)
    base = path.split("/")[-1]
    if base == "Win_98.raw" or base == "Win_2k.img":
        memory = 512
    else:
        memory = 8192
    qemu_system = pexpect.spawn(
        f'bash ./bin/start-test-environment.sh -i {path} -f {imFormat} -p {port} -o "-netdev user,id=n0 -device rtl8139,netdev=n0 -rtc base=localtime" -c {CPU} -m {memory}'
    )
    logger.info(f"\x1b[{INFO_COLOR}mstarting operating system ...\x1b[0m")
    time.sleep(1)
    vncview = subprocess.Popen(["vncviewer", "-Scaling=1920x1080", f"localhost:{port}"])
    time.sleep(4)
    logger.debug(
        f"\x1b[{DEBUG_COLOR}mrecording test session with rfbproxy\x1b[0m"
    )
    logger.debug(
        f"\x1b[{DEBUG_COLOR}mRFB Record: {path}{suffix}.fbs\x1b[0m"
    )
    maxrms = 1000
    rms = compare_screen(port, f"./vnc-macros/loggedIn_{base}.jpg")
    while rms >= maxrms:
        rms = compare_screen(port, f"./vnc-macros/loggedIn_{base}.jpg")
        time.sleep(1)
        print(f"\rRMS: {rms}", end="")
    print("\r", end="")
    rfbProxy = pexpect.spawn(
        f"rfbproxy -s --server=localhost:{port} -r {path}{suffix}.fbs"
    )
    time.sleep(5)
    logger.debug(f"\x1b[{DEBUG_COLOR}mWindows booted\x1b[0m")
    logger.debug(f"\x1b[{DEBUG_COLOR}mTesting Internet Explorer\x1b[0m")
    vnc = pexpect.spawn(f"vncdotool -v -s localhost:{port} ./vnc-macros/open-ie.vdo")
    ie_res = docker.expect(
        [
            ".*POST \/all\/resource\/postreq\?url=https%3A%2F%2Fwww.google.de",
            pexpect.TIMEOUT,
        ],
        testtimeout,
    )
    time.sleep(timebetween)
    if base not in ['Win_98.raw', 'Win_2k.img']:
        pexpect.run(f"vncdotool -s localhost:{port} key super-r pause 1 type \"taskkill \" key shift-7 type \"IM iexplore.exe\" key enter")
    else:
        pexpect.run(f"vncdotool -s localhost:{port} key alt-f4")
    logger.debug(
        f'\x1b[{DEBUG_COLOR}mrestarting pywb ...\x1b[0m'
    )
    docker = pexpect.spawn("docker-compose down")
    docker.expect(pexpect.EOF)
    docker = pexpect.spawn("docker-compose up")
    docker.expect(".*running gevent loop engine.*", timeout=120)
    logger.debug(f"\x1b[{DEBUG_COLOR}mTesting Firefox\x1b[0m")
    vnc = pexpect.spawn(f"vncdotool -v -s localhost:{port} ./vnc-macros/open-firefox.vdo")
    firefox_res = docker.expect(
        [
            ".*POST \/all\/resource\/postreq\?url=https%3A%2F%2Fwww.google.de",
            pexpect.TIMEOUT,
        ],
        testtimeout,
    )
    time.sleep(timebetween)
    if base not in ['Win_98.raw', 'Win_2k.img']:
        pexpect.run(f"vncdotool -s localhost:{port} key super-r pause 1 type \"taskkill \" key shift-7 type \"IM firefox.exe\" key enter")
    else:
        pexpect.run(f"vncdotool -s localhost:{port} key alt-f4")
    chrome_res = 2
    if base in ["Win_10.img", "Win_8.1.img", "Win_XP.img"]:
        logger.debug(
            f'\x1b[{DEBUG_COLOR}mrestarting pywb ...\x1b[0m'
        )
        docker = pexpect.spawn("docker-compose down")
        docker.expect(pexpect.EOF)
        docker = pexpect.spawn("docker-compose up")
        docker.expect(".*running gevent loop engine.*", timeout=120)
        vnc = pexpect.spawn(
            f"vncdotool -v -s localhost:{port} ./vnc-macros/open-chrome.vdo"
        )
        logger.debug(f"\x1b[{DEBUG_COLOR}mTesting Chrome\x1b[0m")
        chrome_res = docker.expect(
            [
                ".*POST \/all\/resource\/postreq\?url=https%3A%2F%2Fwww.google.de",
                pexpect.TIMEOUT,
            ],
            testtimeout,
        )
        time.sleep(timebetween)
        if base not in ['Win_98.raw', 'Win_2k.img']:
            pexpect.run(f"vncdotool -s localhost:{port} key super-r pause 1 type \"taskkill \" key shift-7 type \"IM chrome.exe\" key enter")
        else:
            pexpect.run(f"vncdotool -s localhost:{port} key super-r pause 1 type \"pskill chrome.exe\" key enter")
    if ie_res == 1:
        logger.error("\x1b[31mInternet Explorer did not accept the certificate \x1b[0m")
    elif ie_res == 0:
        logger.info("\x1b[32mInternet Explorer did accept the certificate\x1b[0m")
    if firefox_res == 1:
        logger.error("\x1b[31mFirefox did not accept the certificate \x1b[0m")
    elif firefox_res == 0:
        logger.info("\x1b[32mFirefox did accept the certificate\x1b[0m")
    if chrome_res == 1:
        logger.error("\x1b[31mChrome did not accept the certificate\x1b[0m")
    elif chrome_res == 0:
        logger.info("\x1b[32mChrome did accept the certificate\x1b[0m")
    elif chrome_res == 2:
        logger.info("\x1b[0mChrome is not available on the system\x1b[0m")
    macro = (
        "./vnc-macros/shutdown-old.vdo"
        if base in ["Win_2k.img", "Win_98.raw"]
        else "./vnc-macros/shutdown.vdo"
    )
    logger.debug(f"\x1b[{DEBUG_COLOR}mShutting down the system\x1b[0m")
    vnc = pexpect.spawn(f"vncdotool -v -s localhost:{port} {macro}")
    vnc.expect(pexpect.EOF, timeout=60)
    qemu_system.expect(pexpect.EOF, timeout=900)
    vncview.terminate()
    logger.debug(f"\x1b[{DEBUG_COLOR}mstopping pywb ...\x1b[0m")
    docker = pexpect.spawn("docker-compose down")
    docker.expect(pexpect.EOF, timeout=120)
    rfbProxy.expect(pexpect.EOF)
    f = open(f"{path}.{suffix}", "w")
    f.write(str(ie_res) + "," + str(firefox_res) + "," + str(chrome_res))
    f.close()
    logger.debug('generating viedo from test session')
    logger.debug(f'video path: {path}{suffix}.avi')
    fbs2avi = pexpect.spawn(f'./bin/fbs2video.sh {path}{suffix}.fbs {path}{suffix}.avi')
    fbs2avi.expect(pexpect.EOF, timeout=600)
    print()


def restart_pywb():
    docker = pexpect.spawn("docker-compose down")
    docker.expect(pexpect.EOF)
    docker = pexpect.spawn("docker-compose up")
    docker.expect(".*running gevent loop engine.*", timeout=120)


def test_linux_image(path, imFormat, port=1, verbose=False, suffix=".results"):
    logger.info(f"\x1b[{INFO_COLOR}mtesting image from path {path}:\x1b[0m")
    logger.debug(f"\x1b[{DEBUG_COLOR}mstarting pywb ...\x1b[0m")
    docker = pexpect.spawn("docker-compose up")
    docker.expect(".*running gevent loop engine.*", timeout=120)
    bash_prompt = [".*\$", ".*~>", ".*~#", "~]#", "~ #"]
    child = pexpect.spawn(
        f"bash ./bin/start-test-environment.sh -i {path} -f {imFormat} -c {CPU} -m 8192 -p {port}"
    )
    time.sleep(1)
    vnc = subprocess.Popen(["vncviewer", "-Scaling=1920x1080",  f":{port}"])
    time.sleep(4)
    if os.environ.get("SERIAL", 0) == 1:
        child.logfile = sys.stdout.buffer
    child.expect(".*login:", timeout=900)
    child.sendline("root")
    child.expect("Password:", timeout=60)
    child.sendline("Un!Fr3!burg2019")
    child.expect(bash_prompt, timeout=60)
    child.sendline("startx -- :1 > /dev/null &")
    rfbProxy = pexpect.spawn(
        f"rfbproxy -s --server=localhost:{port} -r {path}{suffix}.fbs"
    )
    logger.debug(
        f"\x1b[{DEBUG_COLOR}mrecording test session with rfbproxy\x1b[0m"
    )
    logger.debug(
        f"\x1b[{DEBUG_COLOR}mRFB Record: {path}{suffix}.fbs\x1b[0m"
    )
    time.sleep(5)
    child.expect(bash_prompt, timeout=60)
    child.sendline('export https_proxy="10.0.2.2:8080"')
    child.expect(bash_prompt, timeout=60)
    logger.debug(f"\x1b[{DEBUG_COLOR}mtesting curl\x1b[0m")
    child.sendline("curl https://www.google.de -o /tmp/curl_res ")
    child.expect(bash_prompt, timeout=60)
    curl_res = docker.expect(
        [
            ".*POST \/all\/resource\/postreq\?url=https%3A%2F%2Fwww.google.de",
            pexpect.TIMEOUT,
        ],
        10,
    )
    logger.debug(
        f'\x1b[{DEBUG_COLOR}mrestarting pywb ...\x1b[0m'
    )
    docker = pexpect.spawn("docker-compose down")
    docker.expect(pexpect.EOF)
    docker = pexpect.spawn("docker-compose up")
    docker.expect(".*running gevent loop engine.*", timeout=120)
    logger.debug(f"\x1b[{DEBUG_COLOR}mtesting firefox\x1b[0m")
    child.sendline("export DISPLAY=:1")
    child.expect(bash_prompt, timeout=60)
    child.sendline("firefox https://www.google.de")
    firefox_res = docker.expect(
        [
            ".*POST \/all\/resource\/postreq\?url=https%3A%2F%2Fwww.google.de",
            pexpect.TIMEOUT,
        ],
        60,
    )
    time.sleep(5)
    child.send(chr(3))
    logger.debug(f"\x1b[{DEBUG_COLOR}mshutting system down ...\x1b[0m")
    child.sendline("shutdown -h now")
    child.expect(pexpect.EOF, timeout=900)
    logger.debug(f"\x1b[{DEBUG_COLOR}mstopping pywb ...\x1b[0m")
    docker = pexpect.spawn("docker-compose down")
    docker.expect(pexpect.EOF, timeout=120)
    vnc.terminate()
    rfbProxy.expect(pexpect.EOF)
    if curl_res == 1:
        logger.error("\x1b[31mCurl did not accept the certificate\x1b[0m")
    elif curl_res == 0:
        logger.info("\x1b[32mCurl did accept the certificate\x1b[0m")
    if firefox_res == 1:
        logger.error("\x1b[31mFirefox did not accept the certificate \x1b[0m")
    elif firefox_res == 0:
        logger.info("\x1b[32mFirefox did accept the certificate\x1b[0m")
    f = open(f"{path}.{suffix}", "w")
    f.write(str(curl_res) + "," + str(firefox_res))
    f.close()
    logger.debug('Generating viedo from test')
    logger.info(f'video path: {path}{suffix}.avi')
    fbs2avi = pexpect.spawn(f'./bin/fbs2video.sh {path}{suffix}.fbs {path}{suffix}.avi')
    fbs2avi.expect(pexpect.EOF, timeout=600)
    print()


# Python ArgumentParser Snippet from
# https://gist.github.com/fomightez/0ff6c709ab99bd626cd67fcab03d3ef2
# ==============================================================================

if __name__ == "__main__" and "__file__" in globals():
    """ This is executed when run from the command line """
    # Code with just `if __name__ == "__main__":` alone will be run if pasted
    # into a notebook. The addition of ` and '__file__' in globals()` is based
    # on https://stackoverflow.com/a/22923872/8508004
    # See also https://stackoverflow.com/a/22424821/8508004 for an option to
    # provide arguments when prototyping a full script in the notebook.
    #  -----------------for parsing command line arguments------------------- #
    parser = argparse.ArgumentParser(
        prog="test-disk-image.py",
        description="Test if the installation of a\
                                     ca certificate is working \
                                     properly. The sript starts\
                                     the image and tests it by\
                                     opening a curl and a \
                                     browser session \
                                     automatically, Analysis of\
                                     the web server log \
                                     will be used to test the\
                                     acceptance of the ca \
                                     certificate.",
    )
    parser.add_argument("imagepath", help="path to the disk image which is being tested")
    parser.add_argument(
        "imageformat",
        help="The guest image format. Currently the ca install\
                              script supports only qcow2 and raw",
    )
    parser.add_argument(
        "vncport",
        help="VNCPORT under which the test environment will be\
                              available during testing",
    )
    parser.add_argument(
        "-s",
        "--result_suffix",
        help="Suffix to append to path for the written test \
                              results",
    )
    parser.add_argument(
        "-w",
        action="store_true",
        help="Define if a windows system is going to be tested.\
                              Since Windows is not configured to have a serial\
                              console, the images are tested via vnc macros",
    )
    parser.add_argument("-v", action="store_true", help="Verbose output of test script")
    # I would also like trigger help to display if no arguments provided
    # because  need at least one for url
    # from http://stackoverflow.com/questions/4042452/\
    # display-help-message-with-python-argparse-when-script-is-\
    # called-without-any-argu
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    if not args.w:
        test_linux_image(
            args.imagepath, args.imageformat, args.vncport, args.v, args.result_suffix
        )
    elif args.w:
        test_windows_image(
            args.imagepath, args.imageformat, args.vncport, args.v, args.result_suffix
        )

# ==============================================================================
