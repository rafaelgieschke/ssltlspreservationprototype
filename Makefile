VENV:=pywb_for_tls_preservation
3RD=thirdparty

.PHONY: clean data lint requirements show-help
.DEFAULT_GOAL := show-help
SHELL := /bin/bash
PATH:=$(PWD)/$(3RD)/miniconda/bin:$(PATH)
################################################################################
# MAKE VARIABLES
################################################################################
BACKUPDIR=/run/media/christop/Privat/Studium/Bachelorthesis/QEMU-img/
TEST_WO_CA=/run/media/christop/Privat/Studium/Bachelorthesis/QEMU-img-test/
TESTDIR=/run/media/christop/SSD/Bachelorthesis/QEMU-img/
################################################################################
# Utility
################################################################################


## Format using black
format:
	black -l 90 -t py38 $$(find . -path ./thirdparty -prune -o -iname "*.py" | grep ".py")

## Run the testsuite
test: clean-test
	@echo -e """ \
	\x1b[33m \
	\n=============================================================================== \
	\n  INSTALLING PROTOTPYE CA CERTIFICATE TO IMAGES\
	\n===============================================================================\
	\x1b[0m\
	"""
	./install-ca-cert-to-images.sh -d $(TESTDIR) 
	# ./test.sh -d $(TEST_WO_CA) -s '.res_before_ca_installation'
	./test.sh -d $(TESTDIR) -s '.results'
	@echo -e """ \
	\x1b[1;33m \
	\n===============================================================================\
	\n  RESETTING TEST IMAGES TO DEFAULT \
	\n=============================================================================== \
	\x1b[0m \
	"""
	./bin/reset-images-to-default.sh -y $(BACKUPDIR) $(TESTDIR) 
	# ./bin/reset-images-to-default.sh -y $(BACKUPDIR) $(TEST_WO_CA)
	find $(TESTDIR) -iname "*.fbs" -exec rm {} \;
	find $(TEST_WO_CA) -iname "*.fbs" -exec rm {} \;
	@rm vnc-macros/*_test.jpg
	

## Remove runtime files
clean:
	find . -name '*.pyc' -exec rm --force {} +
	find . -name '__pycache__' -exec rm -rf --force {} +

## remove files generated during test
clean-test:
	find $(TESTDIR) -iname "*results" -exec rm {} \;
	find $(TESTDIR) -iname "*result_before_ca_installation" -exec rm {} \;
	find $(TESTDIR) -iname "*.fbs" -exec rm {} \;
	find $(TEST_WO_CA) -iname "*results" -exec rm {} \;
	find $(TEST_WO_CA) -iname "*result_before_ca_installation" -exec rm {} \;
	find $(TEST_WO_CA) -iname "*.fbs" -exec rm {} \;

## get condas executables available in path 
available:
	export PATH=$(PWD)/$(3RD)/miniconda/bin:$$PATH



## To clean project state
clean-all: clean clean-test
	rm -rf $(3RD)/miniconda

################################################################################
# Setup
################################################################################

## Install all dependencies
requirements: ./$(3RD)/miniconda/
	cd $(3RD) && ./install_miniconda.sh
	conda env create -f environment.yml

./$(3RD)/miniconda/:
	cd $(3RD) && ./install_miniconda.sh
	conda env create -f environment.yml

################################################################################
# Local Experiments
################################################################################


################################################################################
# Cluster Experiments
############k###################################################################



################################################################################
# Make Selfdocumentation
################################################################################
# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
################################################################################

show-help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=22 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
