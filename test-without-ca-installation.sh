#!/usr/bin/env bash

if [ ! -d ./thirdparty/miniconda/ ]
then
  make requirements
fi

. ./thirdparty/miniconda/bin/activate
conda activate pywb_for_tls_preservation

# rsync -a --progress /run/media/christop/Privat/Studium/Bachelorthesis/QEMU-img/Linux/*.img /run/media/christop/SSD/Bachelorthesis/QEMU-img/Linux/

IMAGEPATH=/run/media/christop/SSD/Bachelorthesis/QEMU-img

for path in $(ls $IMAGEPATH/Windows/*.{img,raw})
do
  format="qcow2"
  if [ "$path" == "/run/media/christop/SSD/Bachelorthesis/QEMU-img/Windows/Win_98.raw" ] ||\
     [ "$path" == "/run/media/christop/SSD/Bachelorthesis/QEMU-img/Windows/Win_XP.img" ]
  then
    format="raw"
  fi
  ./test-disk-image.py "$path" "$format" 2 -w -v -s "result_before_ca_installation"
done

for path in $(ls $IMAGEPATH/Linux/*.img)
do
  format="qcow2"
  if [ "$path" == "/run/media/christop/SSD/Bachelorthesis/QEMU-img/Linux/openSUSE_leap_15.img" ]
  then
    format="raw"
  fi
  ./test-disk-image.py $path $format 1 -v -s ".result_before_ca_installation"
done

cat $IMAGEPATH/{Windows,Linux}/*.result_before_ca_installation

conda deactivate
conda deactivate
