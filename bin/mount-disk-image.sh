#!/usr/bin/env bash

PARAMS=""

echoerr() { echo -e "$@" 1>&2; }

printUsage() {
      cat << EOF
      USAGE: mount-disk-image.sh <flags>
      Flags:
               -i|--image:        Path to image
               -p|--mnt-path      Path the root system of the image should be mounted to
EOF
}

while (( "$#" ))
do
  case "$1" in
    -i|--image)
      IMGPATH="$2"
      shift 2
      ;;
    -p|--mnt-path)
      mntDir="$2"
      shift 2
      ;;
    --) #end of argument parsing
      shift
      break
      ;;
    -*|--*-) # unsupported flags
      printUsage
      exit 1
      ;;
    *) # preserver positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

eval set -- "$PARAMS"

echo "Imageparh: $IMGPATH"
echo "Directory to mount RootFS to: $mntDir"
if  [ "x$IMGPATH" == "x" ] || \
    [ "x$mntDir" == "x" ]
then
  printUsage
  exit 1
fi

# ================================================================================
# ================================================================================
# get root privileges
[ "$UID" -eq 0 ] || exec sudo "$0" "$@" -i $IMGPATH -p $mntDir

# ================================================================================
# functions for mounting stuff 
# ================================================================================
echo "" > .lvmvg 
echo "" > .lvmnbd
echo "" > .lvmraw
# ================================================================================
LVMUSED=""

mountRAW() {
free=$(losetup -f)
losetup $free $IMGPATH
fdisk -l $free 
sectorSize=$(fdisk -l $free | grep Units: | grep -oP "\d{0,} bytes" | grep -oP "\d{0,}")
echo $sectorSize
echo "which Partition is the system's root partition (number):"
part=$(askForNumber)
LVM=$(fdisk -l $free | grep p$part | grep -o "Linux LVM")
if [ "$LVM" == "Linux LVM" ]
then
  mountLVM
  echo $LVMUSED
else
  # echo $(fdisk -l $free | grep ^/dev | grep $part | awk '{print $1,"\011",$3}')
  begin=$(fdisk -o Device,Start -l $free | grep ^/dev | grep p$part | awk -F" " '{print $2}')
  echo "Begin: $begin"
  # calculating offset
  off=$(echo "$begin*$sectorSize" | bc)
  echo "Offset: $off"
  losetup -d $free
  losetup -o $off $free $IMGPATH
  fsType=$(blkid $free | grep -oP "TYPE=\"[^\"]*\"" | grep -oP "\".*\"" | tr -d '"')
  mount $free $mntDir
  echo $free > .lvmraw
fi
}

mountQCOW2(){
modprobe nbd max_part=8
# find free nbd device
for x in /sys/class/block/nbd*
do
  S=$(cat $x/size)
  if [ "$S" == 0 ]
  then
   free=$x
   break
  fi
done
echo $free
dev=/dev/$(basename $free)
echo "dev: $dev"
qemu-nbd -c $dev $IMGPATH
echo $dev > .lvmnbd
fdisk -l -o Device,Size,Type $dev
echo "which Partition is the system's root partition (number):"
part=$(askForNumber)
LVM=$(fdisk -l $dev | grep p$part | grep -o "Linux LVM")
echo "lvm: $LVM"
if [ "$LVM" == "Linux LVM" ]
then
  mountLVM
  echo $LVMUSED
else
  mount $dev\p$part $mntDir
  fsType=$(blkid ${dev}p$part | grep -oP "TYPE=\"[^\"]*\"" | grep -oP "\".*\"" | tr -d '"')
fi
} 

mountLVM() {
  LVMUSED="true"
  lvs
  echo "Which logical volume is the root filesystem in?"
  lv=$(askForString)
  lvmStat=$(lvdisplay | grep -A 5 "LV Name.*$lv" | grep "LV Status.* " | awk '{print $3}')
  lvmDev=$(lvdisplay | grep -B 1 "LV Name.*$lv" | grep "LV Path.* " | awk '{print $3}')
  lvmVG=$(lvdisplay | grep -A 1 "LV Name.*$lv" | grep "VG Name.* " | awk '{print $3}')
  vgchange -ay $lvmVG
  echo $lvmVG > .lvmvg
  mount $lvmDev $mntDir
  ls $mntDir
  fsType=$(blkid $lvmDev | grep -oP "TYPE=\"[^\"]*\"" | grep -oP "\".*\"" | tr -d '"')
}

askForNumber() {
while true
do
  read part
  if [[ $part =~ ^[0-9+]$ ]]
  then
    echo $part
    break
  else
    echoerr "\x1b[31mYou should provide a number (^(0-9)+$)\x1b[0m"
  fi
done
}

askForString() {
while true
do
  read part
  if [[ $part =~ ^[0-9A-Za-z_]+$ ]]
  then
    echo $part
    break
  else
    echoerr "\x1b[31mYou should provide a String (^[0-9A-Za-z\_+]$)\x1b[0m"
  fi
done
}

umountQCOW2(){
qemu-nbd -d /dev/$(basename $free)
}

umountRAW(){
losetup -d $free
echo "freed"
}

mountBtrfsSubvolume(){
  btrfspath=$1
  btrfsdev=$2
  echo $btrfsdev
  subvol=$(cat $mntDir/etc/fstab | grep "/$btrfspath " | awk '{print $4}' | cut -d'/' -f2-)
  if [ "x$subvol" != "x" ]
  then
    echo $subvol
    mount $btrfsdev -o subvol=$subvol $mntDir/$btrfspath
  fi
}


# ================================================================================
# detect image format, delegate to suitable mount functions if the root fs 
# is btrfs we have to look if a root folder is a subfolder to inject the ca cert
# ================================================================================

t=$(file $IMGPATH | cut -d":" -f2 | cut -d " " -f2- | cut -d "," -f1)
qt=$(echo $t | grep -o "QEMU QCOW2")
dt=$(echo $t | grep -o "DOS/MBR boot sector")
echo "qt: $qt"
echo "dt: $dt"
if [ "$t" == "QEMU QCOW2 Image (v3)" ] || [ "$qt" == "QEMU QCOW2" ]
then
  echo "mounting qcow2 image using qemu-nbd"
  type="QCOW2"
  mountQCOW2
  if [ "$fsType" == "btrfs" ]
  then
    mountBtrfsSubvolume var "${dev}p$part"
    mountBtrfsSubvolume etc "${dev}p$part"
    mountBtrfsSubvolume usr/local "${dev}p$part"
    mountBtrfsSubvolume home "${dev}p$part"
    mountBtrfsSubvolume root "${dev}p$part"
  fi
elif [ "$dt" == "DOS/MBR boot sector" ]
then
  type="RAW"
  echo "mounting RAW disk image with losetup"
  mountRAW
  if [ "$fsType" == "btrfs" ]
  then
    mountBtrfsSubvolume var "$free"
    mountBtrfsSubvolume etc "$free"
    mountBtrfsSubvolume usr/local "$free"
    mountBtrfsSubvolume home "$free"
  fi
fi

echo -e """
\x1b[34m
===============================================================================
Successfully mounted the image:               $IMGPATH 
Mount path:                                   $mntDir
device:                                       $(mount | grep "$mntDir " | cut -d " " -f1)
Filesystem:                                   $fsType
Imagetype:                                    $type
===============================================================================
\x1b[0m
"""
