#! /usr/bin/env bash
PARAMS=""

printUsage() {
      cat << EOF
      USAGE: create-certificate.sh <flags>
      Flags:
               -k|--CAkey:        CA's private key 
               -c|--CAcert:       CA's certificate
               -d|--domain:       one single domain atm
               -o|--output:       output directory certificates are stored
      Optional Flags:
               -e|--extfile:      External file for definition of alternative names
EOF
}

while (( "$#" ))
do
  case "$1" in
    -k|--CAkey)
      CAKEY="$2"
      shift 2
      ;;
    -c|--CAcert)
      CACERT="$2"
      shift 2
      ;;
    -d|--domain)
      DOMAIN="$2"
      shift 2
      ;;
    -o|--output)
      OUT="$2"
      shift 2
      ;;
    -e|--extfile)
      EXTFILE="$2"
      shift 2
      ;;
    --) #end of argument parsing
      shift
      break
      ;;
    -*|--*-) # unsupported flags
      printUsage
      exit 1
      ;;
    *) # preserver positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

eval set -- "$PARAMS"

if  [ "x$DOMAIN" == "x" ] || \
    [ "x$CAKEY" == "x" ]  || \
    [ "x$CACERT" == "x" ] || \
    [ "x$OUT" == "x" ]
then
  printUsage
  exit 1
fi

openssl genrsa -out "$OUT/$DOMAIN.key" 4096

# Subject alternative names: https://www.thomas-krenn.com/de/wiki/Openssl_Multi-Domain_CSR_erstellen,
#                            https://www.mobilefish.com/developer/apache/apache_quickguide_install_macos_sierra.html,
#                            https://fabianlee.org/2018/02/17/ubuntu-creating-a-trusted-ca-and-san-certificate-using-openssl-on-ubuntu/

if [ "x$EXTFILE" == "x" ]
then
  echo no external file
  openssl req -new -sha256 -key "$OUT/$DOMAIN.key" -subj "/C=US/ST=CA/O=PyWb Development/CN=$DOMAIN" -out "$OUT/$DOMAIN.csr"
else
  openssl req -new -sha256 -key "$OUT/$DOMAIN.key" -config $EXTFILE -out "$OUT/$DOMAIN.csr"
fi

openssl req -in "$OUT/$DOMAIN.csr" -noout -text

if [ "x$EXTFILE" == "x" ]
then
  echo no external file
  openssl x509 -req -in "$OUT/$DOMAIN.csr" -CA "$CACERT" -CAkey "$CAKEY" -CAcreateserial -out "$OUT/$DOMAIN.crt" -days 3560 -sha256
else
  openssl x509 -req -extensions v3_req -in "$OUT/$DOMAIN.csr" -CA "$CACERT" -CAkey "$CAKEY" -CAcreateserial -out "$OUT/$DOMAIN.crt" -days 3560 -sha256 -extfile $EXTFILE
fi

openssl x509 -in "$OUT/$DOMAIN.crt" -text -noout
