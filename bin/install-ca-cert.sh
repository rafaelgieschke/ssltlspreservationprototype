#!/usr/bin/env bash
# This script injects a ca-cert into a non-running disk image in 
# qcow2 or raw format
# needed software lib32-nss, qemu, vde2, ...

# ==============================================================================
# ==============================================================================

# global Variables
# ==============================================================================



printUsage() {
      cat << EOF
      USAGE: install-ca-cert.sh <flags>
      Flags:
               -i|--image:        Path to image, currently the formats qcow2 and raw are supported
               -c|--cert:         Path to CA Certificate
               -n|--cert-name:    Name of CA Certificate showed in Distribution

      Optional Flags:
               -p|--mount-path:   Path the image should be mounted to. Default is /mnt

EOF
}

while (( "$#" ))
do
  case "$1" in
    -i|--image)
      IMGPATH="$2"
      shift 2
      ;;
    -c|--cert)
      certificate="$2"
      shift 2
      ;;
    -n|--cert-name)
      certName="$2"
      echo $certName
      shift 2
      ;;
    -p|--mount-path)
      mntDir=$2
      echo $mntDir
      shift 2
      ;;
    --) #end of argument parsing
      shift
      break
      ;;
    -*|--*-) # unsupported flags
      printUsage
      exit 1
      ;;
    *) # preserver positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

if [[ -z $mntDir ]]
then
  mntDir=/mnt
fi

eval set -- "$PARAMS"

# check for required parameters
if [ "x$IMGPATH" == "x" ] || \
   [ "x$certificate" == "x" ] || \
   [ "x$certName" == "x" ]
then
  printUsage
  exit 1
fi

# =============================================================================
# Default root / argument check 
# ==============================================================================

# get root privileges
[ "$UID" -eq 0 ] || exec sudo "$0" -i $IMGPATH -c $certificate -n $certName -p $mntDir "$@"

# =============================================================================
# Mount guest root filesystem install ca-cert to 
# ==============================================================================

# Mount / Umount functions for the guest system

./bin/mount-disk-image.sh -i $IMGPATH -p $mntDir

# =============================================================================
# =============================================================================
# =============================================================================
# place the cert to the right direction
# update certificate chains
# this addresses the system tools

# this prevents the update from being delete due to system update
base=$(basename $certificate)
CA_Path=$mntDir/usr/local/share/ca-certificates
ls $mntDir

CERT=$(awk -v RS='-----BEGIN CERTIFICATE-----.*-----END CERTIFICATE-----' 'RT{print RT}' $certificate)

function appendCert {
  certBundle=$1
  present=$(python3 -c """
try:
    bundle=open(\"$certBundle\", encoding=\"utf-8\").read()
except UnicodeDecodeError:
    bundle=open(\"$certBundle\", encoding=\"latin-1\").read()
cert=\"\"\"$CERT\"\"\"
print(cert in bundle)
""")
  if [ "$present" == "False" ]
  then
    echo "appending cert to bundle $certBundle"
    echo -e "# $certName\n$CERT"
    echo -e "# $certName\n$CERT" >> $certBundle
  else
    echo "certificate is already in consolidation"
  fi
}

function copyCertToPath {
  cert=$1
  path=$2
  echo cp $cert $mntDir/$path
  cp $cert $mntDir/$path
  chmod 644 $mntDir/$path 
}


certTestPaths=(usr/local/share/ca-certificates etc/ssl/certs var/lib/ca-certificates/pem)
for path in "${certTestPaths[@]}"
do 
  # ls $mntDir/$path
  if [ -d $mntDir/$path ]
  then
    echo "Found cert path $mntDir/$path"
    copyCertToPath "$certificate" "$path/${base%.*}.pem"
  fi
done


testPaths=(etc/ssl/certs/ca-certificates.crt etc/ssl/certs/ca-bundle.trust.crt etc/ssl/certs/ca-bundle.crt etc/pki/tls/cert.pem var/lib/ca-certificates/ca-bundle.pem)
for path in "${testPaths[@]}"
do
  if [ -f "$mntDir/$path" ]
  then
    echo "found certbundle $path"
    appendCert "$mntDir/$path"
  fi
done

# =============================================================================
# =============================================================================
# =============================================================================
# add certificate to the cert database of firefox, chrome, etc ...
# SHOULD BE LEGACY: THIS IS NOT WANTED BUT TILL NOW NO OTHER METHOD FOR 
#                   CHROME/FF/OTHERBROWSERS ARE KNOWN
# cert8 (legacy - DBM)

function nssCert {
  subpath=$1
  if [ -d $mntDir/$subpath ]
  then
    echo "dir $mntDir/$subpath exists"
    for cDB in $(find $mntDir/$subpath -name "cert8.db")
    do
      echo "Found certdatabase $cDB"
      certdir=$(dirname $cDB)
      echo "directory: $certdir"
      docker run \
        -v "$mntDir:$mntDir" \
        -v "$(dirname $certificate):/certs" \
        libnss\
        certutil -A -n "$certName" -t "TCu,Cu,Tu" -i "/certs/$(basename $certificate)"  -d "dbm:$certdir"
    done
    for cDB in $(find $mntDir/$subpath -name "cert9.db")
    do
      echo "Found certdatabase cert9: $cDB"
      cp $cDB $cDB.orig
      certdir=$(dirname $cDB)
      echo "directory: $certdir"
      certutil -A -n "$certName" -t "TCu,Cu,Tu" -i "$certificate" -d "sql:$certdir"
    done
  fi
}

nssCert "root"
nssCert "home"

# =============================================================================
# unount the disk image
# =============================================================================
./bin/umount-image.sh -p $mntDir

echo -e """
\x1b[32m================================================================================
SUCCESSFULLY INSTALLED THE CA CERTIFICAT TO THE IMAGE

        Imagepath:    $IMGPATH
        Certificate:  $certificate
        Name of Cert: $certName

================================================================================
\x1b[0m
"""
