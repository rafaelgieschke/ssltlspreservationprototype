#!/bin/sh

if [ "x$2" == "x" ]
then
  echo "USAGE: fbs2video.sh <fbsFile> <videoOutPut.avi>"
  exit 1
fi

rfbproxy -x "$1" | ppmtoy4m | ffmpeg -f yuv4mpegpipe -i - -vcodec mpeg4 -g 300 -bf 2 "$2"
