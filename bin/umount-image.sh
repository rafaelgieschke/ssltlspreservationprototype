#!/usr/bin/env bash

PARAMS=""

echoerr() { echo -e "$@" 1>&2; }

printUsage() {
  cat << EOF
USAGE: umount-image.sh <flags>
Flags:
        -p|--mnt-path      Path the root system of the image should be mounted to
EOF
}

while (( "$#" ))
do
  case "$1" in
    -p|--mnt-path)
      mntDir="$2"
      shift 2
      ;;
    --) #end of argument parsing
      shift
      break
      ;;
    -*|--*-) # unsupported flags
      printUsage
      exit 1
      ;;
    *) # preserver positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

eval set -- "$PARAMS"

if [ "x$mntDir" == "x" ]
then
  printUsage
  exit 1
fi

# ================================================================================
# 
# ================================================================================
# get root privileges
[ "$UID" -eq 0 ] || exec sudo "$0" "$@" -p $mntDir



# ================================================================================
# functions for unmounting disk image for formats QCOW2, RAW
# ================================================================================
umountQCOW2(){
  qemu-nbd -d $1 
}

umountRAW(){
losetup -d $1
loop=$(basename $1)
size=$(cat /sys/class/block/$loop/size)
if [ "$size" != 0 ]
then
  echo "umount did not succeed"
  exit 1
fi
echo "freed"
}

umountLVMVG() {
  vg=$(cat .lvmvg)
  vgchange -an $vg
}

getImageType() {
  mntDev=$(mount | grep "$1 " | cut -d " " -f1)
  mntI=$(echo $mntDev | grep -o "loop")
  if [ "$mntI" == "loop" ]
  then
    t="RAW"
  else
    t="QCOW2"
  fi
  echo "$t,$mntDev"
  fsType=$(blkid $mntDev | grep -oP "TYPE=\".*\"" | grep -oP "\".*\"" | tr -d '"')
}

getFsType() {
  echo $(blkid $1 | grep -oP "TYPE=\"[^\"]*\"" | grep -oP "\".*\"" | tr -d '"')
}


# ================================================================================
# logic and delegation 
# ================================================================================
t=$(getImageType $mntDir)
type=$(echo $t | cut -d "," -f1)
mntDev=$(echo $t | cut -d "," -f2)
if [[ -z $mntDev ]]
then
  echo -e "\x1b[31mIT APPEARS THAT NO IMAGE IS MOUNTED ON THIS PATH\x1b[0m"
  exit 1
fi
fsType=$(getFsType $mntDev)
echo "mount Device: $mntDev"
echo "image type: $type"
echo "File system Type: $fsType"


for path in $(mount | grep $mntDir/ | awk '{print $3}')
do
  echo "umount $path"
  umount "$path"
done
umount $mntDir


if [ "$fsType" == "btrfs" ]
then
  btrfs check $mntDev
elif [ "$fsType" == "ext4" ]
then
  fsck.ext4 $mntDev
elif [ "$fsType" == "ext3" ]
then
  fsck.ext3 $mntDev
elif [ "$fsType" == "ntfs" ]
then
  sleep 2
  ntfsfix $mntDev
elif [ "$fsType" == "vfat" ]
then
  fsck.vfat -v -t $mntDev
else
  echo "no suitable block device check for: $fsType"
fi

LVMUSED=""
if [ -s .lvmvg ]
then
  LVMUSED="1"
  echo "LVM was used to mount the filesystem, clean everything"
  umountLVMVG
fi

if [ "$type" == "QCOW2" ]
then
  echo "free qcow2 image on device $mntDev witch qemu-nbd"
  if [ "x$LVMUSED" == "x" ]
  then
    umountQCOW2 $mntDev
  else
    umountQCOW2 $(cat .lvmnbd)
  fi
elif [ "$type" == "RAW" ]
then
  echo "free RAW disk image on device $mntDev with losetup"
  if [ "x$LVMUSED" == "x" ]
  then
    umountRAW $mntDev
  else
    umountRAW $(cat .lvmraw)
  fi
fi

# ==============================================================================
# summary 
# ==============================================================================
echo -e """\x1b[34m
===============================================================================
Successfully unmounted the image mounted on:  $mntDir 
Mount device:                                 $mntDev
Filesystem:                                   $fsType
Imagetype:                                    $type
===============================================================================
\x1b[0m
"""
echo 

