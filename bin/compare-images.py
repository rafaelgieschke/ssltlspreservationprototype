#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math, operator, functools
from PIL import Image


def compareImages(file1, file2):
    i1 = Image.open(file1)
    i2 = Image.open(file2)
    h1 = i1.histogram()
    h2 = i2.histogram()
    rms = math.sqrt(
        functools.reduce(operator.add, map(lambda a, b: (a - b) ** 2, h1, h2)) / len(h1)
    )
    return rms


if __name__ == "__main__":
    import sys

    if len(sys.argv) < 3:
        print("USAGE: compare-images <image1> <image2>")
        sys.exit(1)

    i1, i2 = sys.argv[1:3]
    print(compareImages(i1, i2))
