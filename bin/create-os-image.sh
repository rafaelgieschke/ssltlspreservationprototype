#! /usr/bin/env bash
PARAMS=""

printUsage() {
      cat << EOF
      USAGE: create-os-image.sh <flags>
      Flags:
               -i|--image:        Path to image. (Image is newly created)
               -f|--format:       Format of Image
               -s|--size:         Size of Image (NOTE: FORMAT RAW is exactly this large)
               -d|--install-dvd:  ISO DVD of OS System
               -m|--memory:       Amount of RAM in MB

      Optional Flags:
               -o||--other-args:  string of qemu args e.g. for networking
                                  example: -netdev user,id=n0 -device rtl8139,netdev=n0
EOF
}

while (( "$#" ))
do
  case "$1" in
    -i|--image)
      IMAGE="$2"
      shift 2
      ;;
    -d|--install-dvd)
      INSTALLDVD=$2
      shift 2
      ;;
    -f|--format)
      FORMAT="$2"
      shift 2
      ;;
    -m|--memory)
      M="$2"
      shift 2
      ;;
    -o|--other-args)
      OTHER="$2"
      shift 2
      ;;
    -s|--size)
      SIZE="$2"
      shift 2
      ;;
    --) #end of argument parsing
      shift
      break
      ;;
    -*|--*-) # unsupported flags
      printUsage
      exit 1
      ;;
    *) # preserver positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

eval set -- "$PARAMS"

if  [ "x$FORMAT" == "x" ] || \
    [ "x$IMAGE" == "x" ]  || \
    [ "x$SIZE" == "x" ] || \
    [ "x$INSTALLDVD" == "x" ] || \
    [ "x$M" == "x" ]
then
  printUsage
  exit 1
fi

qemu-img create -f "$FORMAT" "$IMAGE" "$SIZE"
echo qemu-system-x86_64 -enable-kvm -drive file="$IMAGE",format="$FORMAT" -cdrom "$INSTALLDVD" -boot d -smp $NUMCPU -m "$M" "$OTHER"
qemu-system-x86_64 -enable-kvm -drive file=$IMAGE,format=$FORMAT -cdrom $INSTALLDVD -boot d -m $M $OTHER
