const dns = require('dns2')

const fs = require('fs')

const hosts = require('./hosts.json') 

const { exec } = require('child_process')

const dnsMap = jsonToMap(hosts)
const ipMap = new Map()

const http = require('http')

const https = require('https')

const url = require('url')

var HttpsProxyAgent = require('https-proxy-agent');

const httpMessageParser = require('http-message-parser')

const sysCA = require('syswide-cas')
sysCA.addCAs('../pywb/proxy-certs/pywb-ca.pem');

const { constants } = require('crypto')

// =============================================================================

const setIp = (ip) => {
  exec(`ip addr add ${ip}/8 dev lo`, (err, stdout, stderr) => {
    if (err) {
      console.log(`could not assign ip address ${ip} to device`)
      return
    }
  })
}

const unsetIp = (ip) => {
  exec(`ip addr delete ${ip}/8 dev lo`, (err, stdout, stderr) => {
    if (err) {
      console.log(stderr)
      console.log(`could not remove ip address ${ip} from device`)
    }
  })
}

process.on('SIGINT', () => {
  for (var entry of dnsMap.entries()) {
    unsetIp(entry[1])
  }
  process.exit()
})

var i = 10
for (var entry of dnsMap.entries()) {
  i += 1
  ipMap.set(entry[1], entry[0])
  setIp(entry[1])
}
i -= 1

console.log(dnsMap)
console.log(ipMap)
// console.log(i)


// =============================================================================


const  host = 'www.google.de'

const httpsOptions = {
  key: fs.readFileSync(`./${host}.key`),
  cert: fs.readFileSync(`./${host}.crt`),
  secureOptions: constants.SSL_OP_ALL,
  secureProtocol: 'SSLv23_server_method'
}

  const ipv4Check = RegExp(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)

https.createServer(httpsOptions, (req, res) =>{
  let host = req.headers.host
  console.log(host)
  if (ipv4Check.test(host)) {
    console.log("ip host in header")
    let ip = host.split(":")[0].replace("/", "")
    host = ipMap.get(ip)
  }
  console.log(host)
  getProxyResponse(`https://${host}${req.url}`, res)
}
).listen(443)

// =============================================================================

const server = dns.createServer(function(request, send) {
  const response = new dns.Packet(request)

  for (let question of request.questions) {
    entry = "8.8.8.8"
    adr = dnsMap.get(question.name)
    if (adr) {
      entry = adr
    } else {
      entry = '10.0.0.'+i
      // TODO(mytolo): There has to be one line adding the entry to localhost such that the proxy is available
      //               under that address
      dnsMap.set(question.name, entry)
      ipMap.set(entry, question.name)
      setIp(entry)
      i += 1
      fs.writeFileSync("./hosts.json", mapToJSON(dnsMap))
    }

  response.header.qr = 1
  response.answers.push({
    name: question.name,
    address: entry, 
    ttl: 60,
    type: dns.Packet.TYPE.A,
    class: dns.Packet.CLASS.IN
  })

  }

  send(response)
}).listen(8053)

// =============================================================================

function mapToJSON(map) {
  return JSON.stringify([...map])
}

function jsonToMap(json) {
  json = JSON.stringify(json)
  return new Map(JSON.parse(json))
}

const getPywbEntry = async (endpoint, proxyres) => {
  const options = {
    port: 8080,
    host: '127.0.0.1',
    method: 'CONNECT',
    path: `${endpoint}:80`
  }
  const req = http.request(options)
  req.end()
  let entry = ""
  req.on('connect', (res, socket, head) => {
    socket.write(`GET / HTTP/1.1\r\nHost: ${endpoint}\r\nConnection: close\r\n\r\n`)
    let data = []
    socket.on('data', (chunk) => {
      data.push(chunk)
    })
    socket.on('close', () => {
      data = Buffer.concat(data)
      const parsedHTTP = httpMessageParser(data)
      // proxyres.writeHead(result.statusCode, result.headers)
        // proxyres.write(result.bodyData)
      // console.log(parsedHTTP)
      proxyres.writeHead(parsedHTTP.statusCode, parsedHTTP.headers)
      proxyres.write(parsedHTTP.body)
      proxyres.end()
    })
  })
}



var proxy = process.env.http_proxy || 'http://127.0.0.1:8080';
console.log('using proxy server %j', proxy);
 
const getProxyResponse = (endpoint, proxyres) => {
  // HTTPS endpoint for the proxy to connect to
  // console.log('attempting to GET %j', endpoint);
  var options = url.parse(endpoint);
 
  // create an instance of the `HttpsProxyAgent` class with the proxy server information
  var agent = new HttpsProxyAgent(proxy);
  options.agent = agent;
 
  https.get(options, function (res) {
    // console.log('"response" event!', res.headers);
    res.pipe(proxyres || process.stdin)
});
}

