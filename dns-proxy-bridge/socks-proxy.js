const SocksClient = require('socks').SocksClient

const config= {
  proxy: {
    host: '127.0.0.1',
    port: 12345,
    type: 5
  },
  command: 'connect',
  destination: {
    host: 'localhost',
    port: 8080
  }
}




// Promises
SocksClient.createConnection(config)
.then(info => {
  info.socket.on('data', (data) => {
    console.log(data.toString())
  })
  console.log(info.socket.remoteAddress)
  info.socket.write('GET /pywb_recorded HTTP/1.1\n\n')
  console.log(info.socket);
  // <Socket ...>  (this is a raw net.Socket that is established to the destination host through the given proxy server)
})
.catch(err => {
  // Handle errors
});

