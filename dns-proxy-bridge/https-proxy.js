var url = require('url');
var https = require('https');
var http = require('http');
const httpMessageParser = require('http-message-parser')


// used https://www.npmjs.com/package/https-proxy-agent
//  
var HttpsProxyAgent = require('https-proxy-agent');
const sysCA = require('syswide-cas');
sysCA.addCAs('../pywb/proxy-certs/pywb-ca.pem');

// HTTP/HTTPS proxy to connect to
var proxy = process.env.http_proxy || 'http://127.0.0.1:8080';
console.log('using proxy server %j', proxy);
 
const getProxyResponse = (endpoint) => {
  // HTTPS endpoint for the proxy to connect to
  console.log('attempting to GET %j', endpoint);
  var options = url.parse(endpoint);
 
  // create an instance of the `HttpsProxyAgent` class with the proxy server information
  var agent = new HttpsProxyAgent(proxy);
  options.agent = agent;
 
  https.get(options, function (res) {
  console.log('"response" event!', res.headers);
    res.pipe(process.stdout)
});

}

getProxyResponse(process.argv[2] || 'https://www.google.de')




const getPywbEntry = (endpoint) => {
  const options = {
    port: 8080,
    host: '127.0.0.1',
    method: 'CONNECT',
    path: process.argv[2] || 'www.google.de:443'
  }
  const req = http.request(options)
  req.end()
  req.on('connect', (res, socket, head) => {
    // console.log(`GET / HTTP/1.1\r\nHost: ${options.path}\r\nConnection: close\r\n\r\n`)
    let data = []
    socket.write(`GET / HTTP/1.1\r\nHost: ${options.path}\r\nConnection: close\r\n\r\n`)
    socket.on('data', (chunk) => {
      data.push(chunk)
      // console.log(chunk.toString())
    })
    socket.on('close', () => {
      data = Buffer.concat(data)
      console.log(data)
      const httpMessage = httpMessageParser(data)
      console.log(httpMessage.statusCode)
    })
  })
}

// getPywbEntry(process.argv[2] || "https://www.google.de")
