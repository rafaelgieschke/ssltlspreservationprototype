#!/usr/bin/env bash

cd 3rdparty
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
bash miniconda.sh -b -p ./miniconda # Silent mode
rm miniconda.sh
