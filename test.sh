#!/usr/bin/env bash
PARAMS=""

printUsage() {
      cat << EOF
      USAGE: test.sh <flags>
      Flags:
               -d|--image-dir:     path to qemu image root dir with a Linux and a
                                   Windows Folder containing the systems
      Optional:
               -s|--suffix:        suffix for test results. DEFAULT: '.results'
               -p|--vnc-port:      VNC port for qemu vnc server
EOF
}

SUFFIX=".results"
PORT="1"
while (( "$#" ))
do
  case "$1" in
    -s|--suffix)
      SUFFIX="$2"
      shift 2
      ;;
    -p|--vnc-port)
      PORT="$2"
      shift 2
      ;;
    -d|--image-dir)
      IMAGEPATH="$2"
      shift 2
      ;;
    --) #end of argument parsing
      shift
      break
      ;;
    -*|--*-) # unsupported flags
      printUsage
      exit 1
      ;;
    *) # preserver positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

eval set -- "$PARAMS"

if  [ "x$IMAGEPATH" == "x" ]
then
  printUsage
  exit 1
fi

. ./thirdparty/miniconda/bin/activate
conda activate pywb_for_tls_preservation

for path in $(ls $IMAGEPATH/Windows/*.img)
do
  format="qcow2"
  if [ "$(basename $path)" == "Win_98.raw" ] || \
     [ "$(basename $path)" == "Win_XP.img" ]
  then
    format="raw"
  fi
  ./test-disk-image.py "$path" "$format" "$PORT" -w -s "$SUFFIX"
done

exit 0


for path in $(ls $IMAGEPATH/Linux/*.img)
do
  format="qcow2"
  if [ "$(basename $path)" == "openSUSE_leap_15.img" ]
  then
    format="raw"
  fi
  ./test-disk-image.py $path $format "$PORT" -s "$SUFFIX"
done
