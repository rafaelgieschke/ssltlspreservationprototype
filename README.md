*TODO(mytolo): Give detailed information about goals and project description*

# Implementation Guide / Next steps
- [ ] Find a way setting name server for qemu hosts
- [x] Write a test script using pyexpect for linux guest automation, in
      order to work properly, the kernel parameteri `console=ttyS0` has
      to be added to the grub configuration. This can be automated by:
      `sed 's/\(linux[ ]*\/[-_@a-zA-Z .=0-9]*$\)/\1 console=ttyS0/g' -i.bak <pathToGrubCFG>`
- [ ] start x server and set appropriate environment variables:
      `XDG_RUNTIME_DIR=/tmp DISPLAY=:0.0`. Afterwards any graphical
      application like firefox can be opened in tty.
- [ ] Write a test script for windows using vncdo and vnc key macros
- [ ] parse test results and plot it with gnuplot / matplotlib / seaborn
- [ ] The http proxy is currently not working correctly. The ressources 
      are not loaded completely.
      `wbinfo.proxy_magic = pywb.proxy` with the correct socket 
- [x] split the functionality of the bash script `install-ca-cert.sh`,`install-ca-cert-win.sh` into their components:
  - This leads to the following advantages:
  - [x] only variables which are needed are passed
  - [x] components are not redundant in script
  - [x] every component needs a comfortable parsing technique
- [x] add CA Root cert in windows to `HKEY_LOCAL_MACHINE\software\Microsoft\SystemCertificates\ROOT` in order that firefox find the right certificate
- [x] add conda venv for all dependencies for nice shipping
- [x] setup pywb for CA / TLS proxy
- [ ] inject CA cert to different host systems:
  - [ ] Windows:
    - [x] Windows 10
    - [x] Windows 8/8.1 
    - [ ] Windows 7
    - [ ] Windows Vista
    - [x] Windows XP
    - [x] Win 2000
    - [ ] Win 98
    - [ ] Win 95
  - [ ] UNIX:
    - [x] Ubuntu 18.04
    - [x] Ubuntu 16.04
    - [x] Debian 10.1
    - [x] Debian 9
    - [x] Fedora 10 
    - [x] OpenSUSE Tumbleweed 15
    - [x] OpenSUSE LEAP 15
    - [ ] RHEL
    - [ ] CENT OS 6
    - [ ] 
- [x] develop docker container for pywb (available via docker-compose)
- [ ] test images from the chair
- [x] bypass `upadte-ca-certificate`: 
  1. [x] reimplement functionality in Bash <- Just paste CA Certificate to /etc/ssl/certs/ca-certificates.crt bundle, add
  certificate to `/etc/ssl/certs/`, `/usr/local/ca-certificates`
  1. [ ] use qemu-binary-translation
  1. [ ] /etc/inittab, /etc/rc.local, upstart
- [ ] Is it possible to bypass nss? Force modern browser to use system store. This is till now only possible in Windows
  *(TODO(mytolo):Find_out_since_which_ff_version_pref_is_available)*
  - Windows' Firefox enables the usage of the system certificate store by executing this javascript: 

    ```js
    /* Enable experimental Windows trust store support */
    pref("security.enterprise_roots.enabled", true);
    ```
- [x] Install cert without user interaction on Windows e.g. using a service injection toolkit for windows. This is done by hivex and mount of Windows Registry hive `Windows/system32/config/SOFTWARE`.
- [ ] can pywb be configured to generate SSL2 / SSL3 TLS Service certificates
- [ ] maybe there have to be more than one pywb with different openssl versions in order to implement this
- [ ] Test Firefox 2.0 For old Windows
- [ ] Clean ntfs 3g umount, umount <- Wait till ntfs-3g is not present any more
- [ ] write a management script for install ca-cert for raw, qcow2 images and getting certificates for a arbitrary service.
  
  NOTE: prefered way injecting Certificates would be using the offline image of the guest.

# Software used
- `qemu` : Emulating the tested system
- `qemu-nbd` : `qcow2` image file in nbd block device
- `ntfs-3g` : For Windows root partitions
- `losetup` : RAW image file in loop block device
- `lib32-nss` : Installing root ca for firefox,chromium,chrome in Linux / Firefox in Windows

# Software planned to use
- [lklfuse](https://github.com/lkl/linux) mount raw disk images without root privileges
- [fuseqemu](https://gitlab.com/rafaelgieschke/fuseqemu/) mount qemu images without root privileges

# Generall Notes / Ideas:
- using ```curl -x <pywbProxy> <UrlFromCollection>``` in succession with chromium [headless browser](https://chromium.googlesource.com/chromium/src/+/lkgr/headless/README.md) 
could be used for automated testing. A mock html site could then be used to determine if the host accepted the proxy certificate. (Easiest way to do so could be including a frame loading a specific file which could be greped in favour of detecting the acceptance)
- modern Chrome and Firefox use their own CA Trust Storage, see this [chromium readme](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_cert_management.md)
- Windows' Firefox uses the same technique -> This part in linux can be adopted in Windows -> it is even easier in Windows, firefox provides tools configuring firefox to use the system certificate store.
- Use [Replacement of libnssckbi.so](https://www.bachmann-lan.de/linux-mit-eigenen-ssl-zertifikaten-root-ca-installieren/) for Linux Firefox / Chrome

## external Links
- [CA-Rootzertifikat unter Linux und Windows importieren](https://thomas-leister.de/ca-zertifikat-importieren-linux-windows/)
- [CA-Rootzertifikat Windows Command Prompt](http://www.networknet.nl/apps/wp/archives/2272)
- [Windows CA-Dir](https://docs.microsoft.com/en-us/windows/win32/seccertenroll/about-certificate-directory)
- [Windows System Stores Location](https://docs.microsoft.com/en-us/windows/win32/seccrypto/system-store-locations)
- [mount Windows regestry](https://mashtips.com/how-to-edit-the-windows-registry-using-ubuntu/)
- [Arch Package](https://www.archlinux.org/packages/community/x86_64/chntpw/)

# Testing / Getting results
- [Python Module Pexpect](https://pexpect.readthedocs.io/en/stable/overview.html)

# [Windows ISO Images](https://winworldpc.com)

# Windows ca cert stuff
- [Certificate Store](https://docs.microsoft.com/en-us/windows/win32/seccrypto/managing-certificates-with-certificate-stores)
- [Local Machine and Current User Certificate Stores](https://docs.microsoft.com/en-us/windows-hardware/drivers/install/local-machine-and-current-user-certificate-stores)
- [Trusted Root Certification Authorities Certificate Store](https://docs.microsoft.com/en-us/windows-hardware/drivers/install/trusted-root-certification-authorities-certificate-store)
- [Simplified Install of Certificate/Trusted Root on Workstations (via Registry)(https://www.novell.com/coolsolutions/feature/18875.html)
- [hivexregedit](http://libguestfs.org/hivexregedit.1.html)
- [hivex-repo](https://github.com/libguestfs/hivex)
- Windows 2000 does not have the path `Microsoft\SystemCertificates\AuthRoot\Certificates` in the Software Hive of the Windows Registry. Using `Microsoft\SystemCertificates\CA\Certificates` instead.
- [Support for SSL/TLS protocols on Windows](https://blogs.msdn.microsoft.com/kaushal/2011/10/02/support-for-ssltls-protocols-on-windows/)
- [Using windows certificate store in mozilla Firefox](https://www.michaelmiklis.de/using-windows-certificate-store-in-mozilla-firefox/)
- [Configuring Firefox to use the Windows Certificate Store](https://support.umbrella.com/hc/en-us/articles/115000669728-Configuring-Firefox-to-use-the-Windows-Certificate-Store)


# QEMU
- [Windows specific qemu networking](https://wiki.qemu.org/Documentation/Networking#Guest_Hints)
  - WinXP: ```-netdev user,id=n0 -device rtl8139,netdev=n0```
  - Win2000: ```-netdev user,id=n0 -device rtl8139,netdev=n0```
- [Install win95 in qemu](https://wiki.qemu.org/Documentation/GuestOperatingSystems/Windows95)
 

## Bash / Win batch script links
- [awk match Certificate block](https://stackoverflow.com/questions/51690740/grep-to-match-entire-certificate)
- [certutil windows](https://docs.microsoft.com/de-de/windows-server/administration/windows-commands/certutil)
- [batch script request Admin Privileges](https://stackoverflow.com/questions/1894967/how-to-request-administrator-access-inside-a-batch-file)

# Notes
- on arch linux the ca-certificate can be added by placing the pem file into `/etc/ca-certificates/trust-source/anchors/`. Run `update-ca-trust` with root
  privileges afterwards
- automate qemu linux guests (howto)[https://fadeevab.com/how-to-setup-qemu-output-to-console-and-automate-using-shell-script/]
- qemu can handle http/s disk images, http server must be able to handle range requests. No http2.

# DNS PyWb Proxy bridge
- nodejs dns2 packet does not implement [EDNS](https://tools.ietf.org/html/rfc6891),
  dig will print a warning but name resolution still work. (One has to implement
  the changes suggested by this [issue](https://github.com/techguy613/native-dns-packet/blob/master/packet.js))




#### Not sorted:
- [Internet Explorer 8/9: TLS/SSL-Funktionen sind inaktiv](https://support.microsoft.com/de-de/help/2779122)
- [Can serving HTTPS to Internet Explorer on Windows XP be made secure?](https://security.stackexchange.com/questions/82066/can-serving-https-to-internet-explorer-on-windows-xp-be-made-secure)
- [Enabling TLS 1.1 and TLS 1.2 in Internet Explorer](https://support.freshdesk.com/support/solutions/articles/222861-enabling-tls-1-1-and-tls-1-2-in-internet-explorer)
- [Internet Explorer version history](https://en.wikipedia.org/wiki/Internet_Explorer_version_history)
- [Windows backword compatibility | hackernews thread](https://news.ycombinator.com/item?id=13450160)
- [Windows devblog oldnewthing](https://devblogs.microsoft.com/oldnewthing/)
- [Tales of Application Compatibility](http://ptgmedia.pearsoncmg.com/images/9780321440303/samplechapter/Chen_bonus_ch01.pdf)
- make docker image to qcow2: scopeo
